#!/bin/tcsh
 setenv HOME /phenix/u/$LOGNAME 
 source /etc/csh.login
 foreach i (/etc/profile.d/*.csh)
   source $i
 end
 source $HOME/.login


if ( ! ($?_CONDOR_SCRATCH_DIR) ) then
    echo 
    echo "    *** ERROR! ***"
    echo
    echo ' '\${_CONDOR_SCRATCH_DIR} was not defined, changed to local dir
    setenv _CONDOR_SCRATCH_DIR  $workingdir
    echo ${_CONDOR_SCRATCH_DIR}
#    exit
endif



set maindir = /phenix/u/rseidl/spin2/eicsmear
set scriptdir = /phenix/u/rseidl/spin2/eicsmear/newprodtest
cd /phenix/u/rseidl/spin2/eicsmear
source eic22_cshrc.csh

set qbins = ( q2_1_10  q2_10_100  q2_100_1000  q2_1000_100000 q2_0_1 )
set datasets = ( ep_noradcor.18x275 ep_noradcor.18x100 ep_noradcor.10x100 ep_noradcor.5x100  ep_noradcor.5x41 eAu_noradcor.18x100 eAu_noradcor.10x100 eAu_noradcor.5x41  en_noradcor.18x275 en_noradcor.10x100  en_noradcor.5x100 en_noradcor.5x41 )
set datasets2 = ( ep_18x275 ep_18x100 ep_10x100 ep_5x100  ep_5x41 eAu_18x100 eAu_10x100 eAu_5x41  en_18x275 en_10x100  en_5x100 en_5x41 )

if ( $# < 3 ) then 
echo usage $0 energyset q2bin run 
exit
endif

set energyset = $datasets[$1]_$qbins[${2}] 
set energyset2 = $datasets2[$1] 
set qbin = $qbins[$2]
set run = $3
set run2 = `printf %03d $3`


echo "running energyset $energyset with $1 qbin $2 and run $run"

echo "jobtemp is $_CONDOR_SCRATCH_DIR"
mkdir ${_CONDOR_SCRATCH_DIR}/$energyset
cd ${_CONDOR_SCRATCH_DIR}/$energyset

mkdir tmp
pwd

echo "running energyset $energyset with $1 and run $run"
echo " ${energyset}_run${run2}.txt"

ls -ltr ${scriptdir}/inputcards/input.data.${energyset}.eic
echo  "${energyset}_run${run2}.txt" >  tmp/input.data.${energyset}_${run2}.eic
ls -ltr tmp

tail -n +2 ${scriptdir}//inputcards/input.data.${energyset}.eic >>  tmp/input.data.${energyset}_${run2}.eic

cp ${scriptdir}/buildtreelog.C ./
cp ${scriptdir}/genHepMC.C ./
echo " copide rood macros " 

pwd

ls -ltr tmp/
cat  tmp/input.data.${energyset}_${run2}.eic

echo " running pythiaeRHIC next "

pythiaeRHIC --noascii  < tmp/input.data.${energyset}_${run2}.eic   > ${maindir}/files/log/${energyset}_run${run2}.log

echo " finished pythiaeRHIC "

root -l -b -q buildtreelog.C\(\"${energyset}_run${run2}.txt\",\"${maindir}/files/${energyset2}\",\"${maindir}/files/log/${energyset}_run${run2}.log\"\)


echo " finished building root trees" 

root -l -b -q genHepMC.C\(\"${maindir}/files/${energyset2}/${energyset}_run${run2}.root\",\"${maindir}/files/${energyset2}\"\)


#abconv -c ip6_hidiv_275x18 -o ${maindir}/files/${energyset2}/${energyset}_run${run2}_ip6 ${maindir}/files/${energyset2}/${energyset}_run${run2}.hepmc

rm ${energyset}_run${run2}.txt

echo "finished creating hepmc files "
