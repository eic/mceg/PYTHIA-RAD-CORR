#!/bin/tcsh -f

if (! -e /tmp/rseidl) mkdir /tmp/rseidl

if ( $#argv < 4) then
echo usage $0 offset nruns setup qbin 
#echo make sure to create the radgen lookup tables before starting this script.
echo "where setup is 1=ep18x275, 2=ep10x100, 3=ep5x100, 4=ep5x41"
echo "               5=en18x275, 6=en10x100, 7=en5x100, 8=en5x41"
echo where qbin is 1=q2_1_10,  2=q2_10_100,  3=q2_100_1000,  4=q2_1000_100000

# Exiting.
exit
endif


set offset = $1  
set nruns  = $2
set setup  = $3
set qval  = $4
@   end = $offset + $nruns  
set run2 = $offset

foreach run  ( `seq 1 1 $nruns`  )
@ run2 += 1 
set rundir = norad_setup${setup}_run${run2}_${qval}
echo submitting run $run2 ... and $run
mkdir condor/${rundir}
cp runpythia_noradvarq.csh condor/${rundir}/runpythia2.csh
chmod ua+x condor/${rundir}/runpythia2.csh
cp template_var.condor.job condor/${rundir}/condor.job
cd condor/${rundir}/
#~/lib/bin/replace MYDIR /phenix/u/rseidl/spin2/eicsmear/newprodtest/condor/${rundir} condor.job
sed -i "s/MYDIR/\/phenix\/u\/rseidl\/spin2\/eicsmear\/newprodtest\/condor\/$rundir/g" condor.job
#~/lib/bin/replace OFFSET $run2 condor.job
sed -i "s/OFFSET/$run2/g"  condor.job
sed -i "s/QBIN/$qval/g" condor.job
sed -i "s/SETUP/$setup/g" condor.job

condor_submit condor.job
cd ../../
#sleep 5
end
